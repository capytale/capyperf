export const parseCommand = (command: string) => {
  if (!command || command.trim() === '') return { action: '', subcommand: '', data: {} }

  const parts = command.match(/(?:-[a-zA-Z]+|".+?"|\S+)/g) || [] // Retourne un tableau vide si `match` est null
  const action = parts.shift() || '' // Si parts est vide, retourner une chaîne vide pour éviter les erreurs
  const subcommand = parts.shift() || ''
  const data: { [key: string]: string } = {}

  let key: string | null = null
  let valueBuffer: string[] = []

  parts.forEach((part: string) => {
    if (part.startsWith('-')) {
      // Si on a une clé en attente, on stocke le buffer comme valeur complète
      if (key) {
        data[key] = valueBuffer.join(' ')
        valueBuffer = []
      }
      key = part.substring(1) // Retirer le tiret
    }
    else {
      valueBuffer.push(part) // On empile les morceaux de valeur
    }
  })

  // Ajouter la dernière clé et son buffer de valeur
  if (key) {
    data[key] = valueBuffer.join(' ')
  }

  return { action, subcommand, data }
}


