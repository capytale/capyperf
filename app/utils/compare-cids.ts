import { init } from '@paralleldrive/cuid2'

export default () => {
  const cid = init({
    length: 3,
  })

  const i = ref(0)
  const ids = ref([])
  const maxvalues = ref([])

  for (let j = 0; j < 100; j++) {
    while (i.value < 1000000000) {
      i.value++
      const id = cid()
      if (ids.value.includes(id)) {
        console.log('Duplicate found:', id, i.value)
        maxvalues.value.push(i.value)
        break
      }
      ids.value.push(id)

      if (i.value % 100000 === 0) {
        console.log(i.value)
      }
    }
  }

  console.log('Moyenne : ', maxvalues.value.reduce((a, b) => a + b, 0) / maxvalues.value.length)
}
