export const useObjCmd = () => {
  return useState('objCmd', () => ({
    action: '',
    subcommand: '',
    data: {},
  }))
}
