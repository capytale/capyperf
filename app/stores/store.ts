export const useStore = defineStore('store', () => {
  const posts = ref([])
  const shallowPosts = shallowRef(posts)
  const objCmd = ref({
    action: '',
    subcommand: '',
    data: {},
  })

  const shallowObjCmd = shallowRef({
    action: '',
    subcommand: '',
    data: {},
  })

  const getPosts = async () => {
    const data = await $fetch('https://jsonplaceholder.typicode.com/posts')
    posts.value = data
  }

  return { posts, shallowPosts, objCmd, shallowObjCmd, getPosts }
})
