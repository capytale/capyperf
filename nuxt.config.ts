// import { usePreset } from '@primevue/themes'
import Aura from '@primevue/themes/aura'

export default defineNuxtConfig({
  compatibilityDate: '2024-07-30',
  future: { compatibilityVersion: 4 },
  nitro: {
    output: {
      publicDir: 'public',
    }
  },
  dir: {
    public: 'www',
  },
  modules: [
    '@nuxt/eslint',
    '@pinia/nuxt',
    '@primevue/nuxt-module',
    '@nuxtjs/color-mode',
    '@nuxtjs/tailwindcss',
  ],
  css: ['primeicons/primeicons.css'],
  colorMode: {
    classSuffix: '',
  },
  primevue: {
    usePrimeVue: true,
    options: {
      theme: {
        preset: Aura,
        options: {
          darkModeSelector: '.dark',
        },
      },

    },
  },

  eslint: {
    config: {
      stylistic: {
        quotes: 'single',
      },
    },
  },
  devtools: {
    enabled: true,

    timeline: {
      enabled: true,
    },
  },
})
